gitUrl = 'https://harshrs08@bitbucket.org/r1coffeeshop/wiredbraincoffee.git'
gitCredentialsId = 'IN302524'
gitBranch = ''
projectVersion = ''
semVer = ''
commit_id = ''
projectPath = ''
octopusServer = 'https://octopus.nettolicious.com/'
octopusKey = 'API-EP5NNM9MEQ5WI9DPIGUZGHPJEI'

pipeline {
	agent any
	options {
		timestamps()
	}
	parameters { 
		string(name: 'FROM_BRANCH', defaultValue: '', description: 'Manual build against specific branch (blank for default):')
	}
	stages {
		stage ('Checkout code') {
			steps {
				script {
					gitBranch = get_sourceBranch()
				}
				echo "Checking out: ${gitBranch}"
				checkout([
					$class: 'GitSCM',
					branches: [[name: gitBranch]],
					doGenerateSubmoduleConfigurations: false,
					userRemoteConfigs: [[
						credentialsId: gitCredentialsId,
						url: gitUrl
					]]
				])
			}
		}
		stage ('Calculate Version') {
			steps {
				get_version()
			}
		}
		stage ('Build') {
			steps {
				script {
					projectPath = "";
					dir("${projectPath}"){
						bat "MyBatchFile.bat ${projectVersion} EXIT %ERRORLEVEL%"
					}
				}
			}
		}
		
		stage ('Create Artifacts') {
			steps {
				script {
					def artifactLocation = ""
					def artifactName = "wiredbraincoffee.${projectVersion}"
					zip archive: true, dir: "${artifactLocation}", glob: '', zipFile: "${artifactName}.zip"
					def server = Artifactory.server 'artifactory-1'
					def uploadSpec = """{
							"files": [
							{
								"pattern": "${artifactName}.zip",
								"target": "build-artifacts/${env.JOB_NAME}/${projectVersion}/"
							}
						]
					}"""
					server.upload spec: uploadSpec, failNoOp: true
				}
			}
		}
		stage('Store Artifacts in Octopus') {
      when { expression { !isPullRequest() } }
      steps {
        uploadToOctopus()
      }
    }
	}
	post {
		always {
			echo 'Build Finished'
		}
		changed {
			echo 'Build status changed'
		}
		fixed {
			echo 'Build is now stable'
			notify_recipients(true)
		}
		regression {
			echo 'Build was successful before but is now failing'
		}
		aborted {
			echo 'Build was manually aborted'
		}
		failure {
			script {
				echo "Build Failed"
				notify_recipients(false)
			}
		}
		success {
			echo 'Build was successful'
			echo 'Cleaning up the workspace'
			cleanWs()
		}
		unstable {
      echo 'Build is Unstable'
		}
		unsuccessful {
			echo 'Build was something other than success'
		}
		cleanup {
			echo 'Doing cleanup in success handler'
		}
	}
}

def get_sourceBranch() {
	if (params.FROM_BRANCH != '') {
		return "*/${params.FROM_BRANCH}"
	}

	echo "env.GIT_BRANCH: ${env.GIT_BRANCH}"
	echo "env.GIT_LOCAL_BRANCH: ${env.GIT_LOCAL_BRANCH}"

	// detect branch from environment
	def branch = env.GIT_BRANCH

	// fall-back to master
	if (branch == null) {
		branch = 'master'
	}
	branch = branch.replace('origin/', '')
	echo "branch: ${branch}"

	return "*/${branch}"
}

def get_version() {
	def gitVersionPayload = bat label: '', returnStdout: true, script: '@echo off & GitVersion'
	echo "GitVersion payload: ${gitVersionPayload}"

	def payload = readJSON text: gitVersionPayload
	semVer = payload.MajorMinorPatch

	projectVersion = semVer.trim() + ".${BUILD_NUMBER}"
	echo "Project version ${projectVersion}"	

	return projectVersion
}

def notify_recipients(isSuccess = true, additionalInfo = "") {
	def recipientProviders = [ requestor() ];
	def directRecipients = ""
	if (isSuccess) {
		directRecipients = "alm@r1rcm.com,emdteam@r1rcm.com"
	}
	else {
		recipientProviders.add(culprits())
		recipientProviders.add(developers())
		directRecipients = "product-tech-amogha@R1RCM.COM,product-tech-brahmos@R1RCM.COM,Product-Tech-Hawkeye@R1RCM.COM,product-tech-team-404@R1RCM.COM"
	}

	def status = isSuccess ? 'SUCCESS' : 'FAILURE'

    emailext (
            to: directRecipients,
			recipientProviders: recipientProviders,
            subject: "${status}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
            body: """<p>${status}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
                <p>Check console output at <a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a></p>
				<p>${additionalInfo}</p>""",
        )
}

def isPullRequest() {
	return env.BITBUCKET_TARGET_BRANCH;
}

def uploadToOctopus() {
  bat "octo push --server ${octopusServer} --package wiredbraincoffee.${projectVersion}.zip --apiKey ${octopusKey}"
}